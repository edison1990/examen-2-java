/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.aiep.Sistema_Veterinaria.controlador;

import cl.aiep.Sistema_Veterinaria.conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author edison
 */
public class IngresoClientes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet IngresoClientes</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet IngresoClientes at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        int rut = Integer.parseInt(request.getParameter("rut"));
        String nombre = request.getParameter("nombre");
        String apepat = request.getParameter("apepat");
        String apemat = request.getParameter("apemat");
        String direccion = request.getParameter("direccion");
        
      
        Conexion cnx = new Conexion();
        
   
        try {
            cnx.getConnection();
             String query = " INSERT INTO Cliente(CliRut, CliNombre, CliApeliidoPaterno, CliApellidoMaterno, CliDireccion) values ( "+rut+",'"+nombre+"', '"+apepat+"','"+apemat+"','"+direccion+"')";
       
        cnx.ejecutar(query);
        response.sendRedirect("IngresoCliente.jsp");
            
           
            
        } catch (SQLException ex) {
            Logger.getLogger(IngresoClientes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(IngresoClientes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(IngresoClientes.class.getName()).log(Level.SEVERE, null, ex);
        }
       
             
               
        
        
    }     
              
        
             
       
 

  
    
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
