<%-- 
    Document   : IngresoMascota
    Created on : 14-09-2019, 10:12:37
    Author     : edison
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="cl.aiep.Sistema_Veterinaria.conexion.Conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
       Conexion cnx = new Conexion();
       cnx.getConnection();
     
    %>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
         <div class="container col-lg-5">
      
      
      <br>
                    <h2>Ingreso de Mascotas</h2>
        <br>            
                    <form action="IngresoMascotas" method="POST">
                        
                        <div class="form group">
                           
                             <%


                ResultSet result =  cnx.ejecutarSelect("select Id , CONCAT(CliNombre, ' ', CliApeliidoPaterno) As Nombre From Cliente"); %>


                                <span>Dueño de la Mascota: </span>
                                <select class="btn btn-info dropdown-toggle" name="dueno">
            
                                 <option value="0">Seleccione nombre del responsable de la mascota</option>
            
 
                    <%
                        while( result.next()){
                            out.println("<option value='"+result.getInt(1)+"'>");
                                out.println(result.getString(2));
                            out.println("</option>");
                        }  %>

                                </select>
                            
                            
                            
                            
                        </div><br>
                        
                        <div class="form group">
                            <span class="label"> Nombre: </span>
                            <input class="form-control" type="text" name="nombre" placeholder="Nombre de la mascota">
                        </div><br>
                        
                         <div class="form group">
                            <span class="label"> Raza: </span>
                            <input class="form-control" type="text" name="raza" placeholder="Raza de la mascota">
                        </div><br>
                        
                         <div class="form group">
                            <span class="label"> Especie: </span>
                            <input class="form-control" type="text" name="especie" placeholder="Tipo de especie (Canino, Felino, ave, etc...)">
                        </div><br>
                        
                        
                        
                        
                        <div class="form group"> 
                            <input class="btn btn-success btn-block" type="submit" name="boton" value="Guardar Mascota">
                        </div>
                       
                        
                    </form> 
        
    </body>
     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
</html>
