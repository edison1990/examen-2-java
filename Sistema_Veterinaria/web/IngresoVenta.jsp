<%-- 
    Document   : IngresoVenta
    Created on : 14-09-2019, 12:35:36
    Author     : edison
--%>

<%@page import="cl.aiep.Sistema_Veterinaria.conexion.Conexion"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
   Conexion cnx = new Conexion();
   cnx.getConnection();
   ResultSet rs;
   String query= "select * from Vista_Venta"; //"Select id, nombre, apellido, edad, direccion from persona";
   rs=cnx.ejecutarSelect(query);
%>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
       
    </head>
    <body><br>
        
        <div class="container">
      &nbsp; &nbsp; &nbsp;  
     
     <h1> Ingreso de Ventas  </h1><br>
    
            
            <form action="IngresoVenta" method="POST">
                
                
                    <span class="label"> Folio: </span>
                    <input type="text" name="folio" placeholder="Folio">
               
                 &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                 
                  <span> Fecha: </span> &nbsp; &nbsp; 
                  <input type="DATE" name="fecha"><br><br>
                
                     <%


                ResultSet result =  cnx.ejecutarSelect("select Id , CONCAT(CliNombre, ' ', CliApeliidoPaterno) As Nombre From Cliente"); %>


                <span>Dueño de la Mascota: </span> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; Nombre Mascota:      <br>  
                                <select class="btn btn-info dropdown-toggle" name="cliente">
            
                                 <option value="0">Seleccione nombre del responsable de la mascota</option>
            
 
                    <%
                        while( result.next()){
                            out.println("<option value='"+result.getInt(1)+"'>");
                                out.println(result.getString(2));
                            out.println("</option>");
                        }  %>

                                </select>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
            
                
               
                     <%


                ResultSet result2 =  cnx.ejecutarSelect("select Id , CONCAT(MasNombre, '- ', MasEspecie) As NombreM From Mascota"); %>

 
                                
                                <select class="btn btn-info dropdown-toggle" name="mascota">
            
                                 <option value="0">Seleccione nombre del responsable de la mascota</option>
            
 
                    <%
                        while( result2.next()){
                            out.println("<option value='"+result2.getInt(1)+"'>");
                                out.println(result2.getString(2));
                            out.println("</option>");
                        }  %>

                                </select><br><br>
                         &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; 
 &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; 

 <button type="reset" class="btn btn-success">Limpiar Campos</button>
                         &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                         <input class="btn btn-danger" type="submit" name="boton" value="Guardar Venta">
              
            </form>
                        
                        
                        
                        <br><br>
                        
                         <table class="table table-bordered"  id="tablaDatos" border="1">
                    <thead>
                        <tr>
                           
                            <th class="text-center">Folio</th>
                            <th>Fecha</th>
                            <th class="text-center">Cliente</th>
                            <th class="text-center">Mascota</th>
                           
                        </tr>
                    </thead>
                    <tbody id="tbodys">
                        <%
                            while (rs.next()) {
                        %>
                        <tr>
                            
                            <td class="text-center"><%= rs.getInt("Folio")%></td>
                            <td><%= rs.getString("Fecha")%></td>
                            <td class="text-center"><%= rs.getString("Cliente")%></td>
                             <td class="text-center"><%= rs.getString("Mascota")%></td>
                            <td class="text-center">
                                
                                <a href="DetalleVenta.jsp?Id=<%= rs.getInt("Id")%>" class="btn btn-primary">Ver detalle venta</a>
                                
                                
                                <!-- <input type="hidden" value="<//%= rs.getInt("Id_Usuario")%>" id="Editar"/>
                                <input type="submit" class="btn btn-warning" data-toggle="modal" data-target="#myModal1" value="Editar"/>  -->
                                
                            </td>
                        </tr>
                        <%}%>
                </table>
      
   </div>
        
        
    </body>
    
     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</html>
