create DATABASE TIENDA_DE_MASCOTAS;

USE TIENDA_DE_MASCOTAS;

Create TABLE Cliente(
Id INT PRIMARY KEY AUTO_INCREMENT,
CliRut INT,
CliNombre VARCHAR(50),
CliApeliidoPaterno VARCHAR(50),
CliApellidoMaterno VARCHAR(50),
CliDireccion VARCHAR(50)
);

select * from Cliente;
select * from Mascota;
select * from mascota;


CREATE TABLE Mascota(
Id INT PRIMARY KEY AUTO_INCREMENT,
MasNombre VARCHAR(50),
MasRaza VARCHAR(50),
MasEspecie VARCHAR(50),
IdCliente INT,
FOREIGN KEY (IdCliente) REFERENCES Cliente(Id)
);

CREATE TABLE Producto(
Id INT PRIMARY KEY AUTO_INCREMENT,
ProNombre VARCHAR(50),
ProCodigo INT,
ProPrecio INT
);

insert into Producto (ProNombre,ProCodigo,ProPrecio) values ('collar de paseo',10001, 2000);
insert into Producto (ProNombre,ProCodigo,ProPrecio) values ('collar anti pulgas',10002, 2900);
insert into Producto (ProNombre,ProCodigo,ProPrecio) values ('alimento',10003, 17000);

Create TABLE Venta(
Id INT PRIMARY KEY AUTO_INCREMENT,
Folio INT,
VenFecha DATE,
IdMascota INT,
FOREIGN KEY(IdMascota) REFERENCES Mascota(Id),
IdCliente INT,
FOREIGN KEY(IdCliente) references Cliente(Id)
);

create TABLE Detalle_Venta(
Id INT PRIMARY KEY AUTO_INCREMENT,
DetCantidad INT,
DetPrecioUnitario INT,
DetPrecioTotal INT,
IdProducto INT,
FOREIGN KEY (IdProducto) REFERENCES Producto(Id),
IdVenta INT,
FOREIGN KEY (IdVenta) REFERENCES Venta(Id)
);

alter VIEW Vista_Venta
AS
  SELECT Venta.Id as Id, Venta.Folio as Folio, Venta.VenFecha as Fecha, Cliente.CliNombre as Cliente, Mascota.MasNombre as Mascota FROM Venta
  INNER JOIN Cliente ON Venta.IdCliente = Cliente.Id
  INNER JOIN Mascota ON Venta.IdMascota = Mascota.Id;
  
  select * from Vista_Venta